var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
var ObjectId = require('mongodb').ObjectId;

var path = require('path');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var port = process.env.port || 1337;
var dbo;

init();

async function init() {


    app.get('/', (req, res) => {
        res.sendFile(path.join(__dirname, '/index.html'));
    });

    app.get("/tasks", async (req, res) => {
        res.json(await findDocuments(dbo));
    });

    app.get("/task/:id", async (req, res) => {
        res.json(await findDocument(dbo, req.params.id));
    });

    dbo = await connect();
    await findDocuments(dbo);


   io.on('connection', (socket) => {
        console.log("client connected");
        socket.on('chat message', async (msg) => {
            socket.broadcast.emit('chat message', msg);
            await dbo.collection('chatbox').insertOne({ 'msg': msg.val , 'naam':msg.naam});
        });
    });

    http.listen(port,() => console.log(`NTask API - Port ${port}`));
    //app.listen(port, () => console.log(`NTask API - Port ${port}`));

    console.log(`Request with Express on Port ${port}`);

}

async function connect() {
    // 1. Connect to MongoDB instance running on localhost

    // Connection URL
    var url = 'mongodb://localhost:27017';

    var db = await MongoClient.connect(url);
    console.log("Connected successfully to server");
    var dbo = db.db("test");
    return dbo;

}

// 3. Query Collection
async function findDocuments(dbo) {
    // Get the documents collection
    const collection = dbo.collection('chatbox');
    const docs = await collection.find({}).toArray();
    console.log("Found the following records");
    console.log(docs)
    return docs;
}

// 3. Query Collection
async function findDocument(dbo, id) {
    // Get the documents collection
    const collection = dbo.collection('chatbox');
    const docs = await collection.findOne({ "_id": new ObjectId(id) });
    console.log("Found the following records");
    console.log(docs)
    return docs;
}